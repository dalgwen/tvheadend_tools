#!/usr/bin/env python
# -*- coding: utf8 -*-

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import sys, re, argparse, requests, json

parser = argparse.ArgumentParser(description = 'Automatics xmltvids find for tvheadhend 4.0 channel')
parser.add_argument('--hts-url', default = 'http://localhost:9981/', help = 'hts server url (default : http://localhost:9981/)')
parser.add_argument('--hts-user', default = 'admin', help = 'hts user (default admin)')
parser.add_argument('--hts-password', default = 'admin', help = 'hts password (default admin)')

args = parser.parse_args()

r = requests.post(
    args.hts_url+'api/channel/grid',
    auth=(args.hts_user, args.hts_password),
    data={'limit':100000}
  )
if r.ok :
    channels = r.json()
else :
    print 'Error: can\'t load channels list'

r = requests.get(
    args.hts_url+'api/epggrab/channel/list',
    auth=(args.hts_user, args.hts_password),
    data={'limit':100000}
  )
if r.ok :
    xmltvids = r.json()
else :
    print 'Error: can\'t load xmltvid list'

pattern = re.compile('^(.*): ([0-9A-Za-z\.\-_]*) \(XMLTV:.*\)\)$')
for ch in channels['entries'] :
    if ch['epggrab'] :
        print '%(name)s already have xmltvid, pass'%ch
    else :
        print 'Search xmltvid for %(name)s'%ch
        found = False
        for xmltvid in xmltvids['entries'] :
            r_search = pattern.search(xmltvid['text'])
            if r_search :
				chan_name = r_search.group(1)
				if chan_name.lower() == ch['name'].lower() :
					print "  - found (%s)"%xmltvid['key']
					found = True
					r = requests.post(
						args.hts_url+'api/idnode/save',
						auth=(args.hts_user, args.hts_password),
						data={
							'node': json.dumps({
								"epggrab":[r_search.group(2)],
								"uuid":ch['uuid']
							  })
						  }
					  )
					if r.ok :
						print "  - saved"
					else :
						print "  - error"
            else :
                print "  - search error (%s)"%xmltvid['val']
        if not found :
            print "  - no found"
